#!/bin/sh -x

set -e

SVERSION=3.4.0
DVERSION=3.4.0
PLATFORM=x86_64-el9-gcc13-opt

mkdir -p build
cd build
cmake -DCMAKE_INSTALL_PREFIX=../${DVERSION}/${PLATFORM} ../Catch2-${SVERSION}/
make clean
make -j 8
make install
cd ..
